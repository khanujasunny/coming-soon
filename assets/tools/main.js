$(document).ready(function () {

    try {
        var capitalize = function (s) {
            if (typeof s !== 'string') return ''
            return s.charAt(0).toUpperCase() + s.slice(1)
        }
        var hostname = window.location.hostname.split('.');
        hostname.pop();
        hostname = hostname.join("-");
        hostname = hostname.replace(/-([a-z])/g, function (g) {
            return " " + g[1].toUpperCase();
        });
        if (hostname) {
            hostname = hostname.replace('www', '');
            document.title = capitalize(hostname) + ": Coming Soon!";
            $('#logo').html(capitalize(hostname)).show();
        }
    } catch (e) {

    }
});